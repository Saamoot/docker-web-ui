<?php

if (false === function_exists('class_basename')) {
    /**
     * @param string $fqcn
     *
     * @return string
     */
    function class_basename($fqcn)
    {
        if (true === is_object($fqcn)) {
            $fqcn = get_class($fqcn);
        }

        $fqcnParts = preg_split('/\/', $fqcn, -1, PREG_SPLIT_NO_EMPTY);

        return end($fqcnParts);
    }
}

if (false === function_exists('parse_size')) {
    function parse_size($size)
    {
        if ('N/A' === $size) {
            return [
                'unit' => null,
                'size' => null,
            ];
        }

        $unit = preg_replace('/[^a-zA-Z]+/', '', $size);
        $size = preg_replace('/[^0-9,. ]+/', '', $size);

        return [
            'unit' => $unit,
            'size' => $size,
        ];
    }
}

if (false === function_exists('pull_value_from_array')) {
    function pull_value_from_array(&$array, $index, $fallbackValue = null)
    {
        if (false === array_key_exists($index, $array)) {
            return $fallbackValue;
        }

        $value = $array[$index];
        unset($array[$index]);

        return $value;
    }
}

if (false === function_exists('create_api_route')) {
    function create_api_route($name, $controller)
    {
        $nameSingular = Str::singular($name);

        Route::get('/' . $name . '/list', [$controller, 'list'])
            ->name($name . '.list');

        Route::get('/' . $name . '/inspect/{' . $nameSingular . '?}', [$controller, 'inspect'])
            ->name($name . '.inspect');

        Route::delete('/' . $name . '/remove/{' . $nameSingular . '?}', [$controller, 'remove'])
            ->name($name . '.remove');

        return Route::resource($name, $controller)
            ->except(['update', 'edit', 'create']);
    }
}