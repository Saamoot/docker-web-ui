<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

trait Uuid
{
    public static function boot()
    {
        parent::boot();

        $creationCallback = function ($model) {
            if (true === empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::orderedUuid()->toString();
            }
        };

        static::creating($creationCallback);
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
