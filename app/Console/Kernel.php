<?php

namespace App\Console;

use DateTime;
use DateTimeZone;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $dateTime = new DateTime('now', new DateTimeZone('Europe/Bratislava'));
            $message  = 'Schedule run @ ' . $dateTime->format('Y-m-d H:i:s') . PHP_EOL;

            $cronLogFile = storage_path() . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'cron';
            file_put_contents($cronLogFile, $message, FILE_APPEND);
        })->everyMinute();

        $this->__duiSaveCommand($schedule, 'dui:save-images');
        $this->__duiSaveCommand($schedule, 'dui:save-volumes');
        $this->__duiSaveCommand($schedule, 'dui:save-networks');
        $this->__duiSaveCommand($schedule, 'dui:save-containers');
        $this->__duiSaveCommand($schedule, 'dui:save-containers-networks');
        $this->__duiSaveCommand($schedule, 'dui:save-containers-volumes');
        $this->__duiSaveCommand($schedule, 'dui:save-containers-images');
    }

    /**
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @param string                                  $artisanCommand
     *
     * @return void
     */
    private function __duiSaveCommand(&$schedule, $artisanCommand)
    {
        $schedule->command($artisanCommand)
            ->everyMinute()
            ->runInBackground()
//            ->evenInMaintenanceMode()
            ->withoutOverlapping(2)
        ;
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
