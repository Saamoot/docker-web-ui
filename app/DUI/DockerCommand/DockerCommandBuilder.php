<?php

namespace App\DUI\DockerCommand;

use App\DUI\DockerCommand\Traits\DockerCommandBuilderProperties;
use App\DUI\CommandExec;

class DockerCommandBuilder
{
    use DockerCommandBuilderProperties;

    static public function getInstance()
    {
        return new static();
    }

    public function build()
    {
        $baseCommandFormat         = $this->getBaseCommandFormat();
        $options                   = $this->buildOptions();
        $containerRuntimeArguments = $this->buildRuntimeArguments();

        return strtr(
            $baseCommandFormat,
            [
                '%containerRuntime'          => $this->getContainerRuntime(),
                '%containerRuntimeArguments' => implode(' ', $containerRuntimeArguments),
                '%subCommand'                => $this->getSubCommand(),
                '%subCommandAction'          => $this->getSubCommandAction(),
                '%options'                   => implode(' ', $options),
                '%arguments'                 => implode(' ', $this->getArguments()),
            ]
        );
    }

    public function toArray()
    {
        return [
            'containerRuntime' => $this->getContainerRuntime(),
            'subCommand'       => $this->getSubCommand(),
            'subCommandAction' => $this->getSubCommandAction(),
            'options'          => $this->getOptions(null, []),
            'arguments'        => $this->getArguments(),
        ];
    }

    static public function createInstanceFromArray($commandArray)
    {
        $instance = new static();

        return $instance->fromArray($commandArray);
    }

    public function fromArray($commandArray)
    {
        foreach ($this->toArray() as $key => $value) {
            if (false === array_key_exists($key, $commandArray)) {
                continue;
            }

            call_user_func([$this, 'set' . ucfirst($key)], $value);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->build();
    }

    /**
     * @return string
     */
    private function getBaseCommandFormat()
    {
        $result = ['%containerRuntime'];

        if (true === $this->hasContainerRuntimeArguments()) {
            $result[] = '%containerRuntimeArguments';
        }

        array_push(
            $result,
            '%subCommand',
            '%subCommandAction',
        );

        if (true === $this->hasOptions()) {
            $result[] = '%options';
        }

        if (true === $this->hasArguments()) {
            $result[] = '%arguments';
        }

        return implode(' ', $result);
    }

    /**
     * @return \App\DUI\DockerCommand\DockerCommandBuilder
     * @throws \Exception
     */
    public function setJsonAsOutputFormat()
    {
        return $this->addOption('format', '"{{ json .}}"');
    }

    /**
     * @return \App\DUI\DockerCommand\DockerCommandBuilder
     * @throws \Exception
     */
    public function setNoTruncOption()
    {
        return $this->addOption('no-trunc');
    }

    /**
     * @return \App\DUI\CommandExec
     */
    public function execute()
    {
        return CommandExec::executeString($this->build());
    }

    /**
     * @return array
     */
    private function buildOptions()
    {
        $result = [];

        foreach ($this->getOptions() as $optionName => $addedOptions) {
            $sanitizedOptionName = DockerCommandArguments::sanitizeKey($optionName);

            foreach ($addedOptions as $addedOption) {
                $formatString = '--%s';

                if ('flags' === $optionName) {
                    $result[] = sprintf($formatString, $addedOption);

                    continue;
                }

                if (false === is_null($addedOption)) {
                    $formatString .= ' %s';
                }

                $result[] = sprintf($formatString, $sanitizedOptionName, $addedOption);
            }
        }

        return $result;
    }

    public function buildRuntimeArguments()
    {
        $result = [];

        foreach ($this->getContainerRuntimeArguments() as $argumentName => $argumentValue) {
            $formatString = '-%=%s';

            if (mb_strlen($argumentName) > 1) {
                $formatString = '-' . $formatString;
            }

            $result[] = sprintf($formatString, $argumentName, $argumentValue);
        }

        return $result;
    }
}