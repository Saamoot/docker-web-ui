<?php

namespace App\DUI\DockerCommand\SubCommands;

use App\DUI\DockerCommand\DockerCommand;

class DockerImageCommand extends DockerCommand
{
    public static function parseHttpRequestToDockerCommandBuilder($requestData)
    {
        $requestData['subCommand'] = 'image';

        return parent::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function pullImage($requestData)
    {
        $requestData['subCommandAction'] = 'pull';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function removeImage($requestData)
    {
        $requestData['subCommandAction'] = 'rm';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function listImages($requestData)
    {
        $requestData['subCommandAction'] = 'ls';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function inspectImage($requestData)
    {
        $requestData['subCommandAction'] = 'inspect';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }
}