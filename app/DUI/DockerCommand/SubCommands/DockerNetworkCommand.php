<?php

namespace App\DUI\DockerCommand\SubCommands;

use App\DUI\DockerCommand\DockerCommand;

class DockerNetworkCommand extends DockerCommand
{
    public static function parseHttpRequestToDockerCommandBuilder($requestData)
    {
        $requestData['subCommand'] = 'network';

        return parent::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function createNetwork($requestData)
    {
        $requestData['subCommandAction'] = 'create';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function removeNetwork($requestData)
    {
        $requestData['subCommandAction'] = 'rm';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function listNetworks($requestData)
    {
        $requestData['subCommandAction'] = 'ls';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function inspectNetwork($requestData)
    {
        $requestData['subCommandAction'] = 'inspect';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function connect($requestData)
    {
        $requestData['subCommandAction'] = 'connect';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function disconnect($requestData)
    {
        $requestData['subCommandAction'] = 'disconnect';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    /**
     * @param mixed       $value
     * @param string|null $field Available fields: dangling, driver, id, label, name, scope, type
     *
     * @return \App\DUI\DockerCommand\DockerCommandBuilder
     */
    public static function filterNetworksBy($value, $field = null)
    {
        if (true === is_null($field)) {
            $field = 'name';
        }

        $requestData['options'] = [
            'filter' => sprintf('"%s=%s"', $field, $value),
        ];

        return static::listNetworks($requestData);
    }

    public static function isContainerInNetwork($requestData)
    {
        $localRequestData = [
            'arguments' => [
                'network' => $requestData['arguments']['network'],
            ],
        ];

        $output = static::inspectNetwork($localRequestData)
            ->setJsonAsOutputFormat()
            ->execute()
            ->getLastLine();

        $output = json_decode($output, true);

        if (false === array_key_exists('Containers', $output)) {
            return false;
        }

        foreach ($output['Containers'] as $containerData) {
            if ($requestData['arguments']['container'] === $containerData['Name']) {
                return true;
            }
        }

        return false;
    }
}