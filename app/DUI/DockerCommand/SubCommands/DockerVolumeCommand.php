<?php

namespace App\DUI\DockerCommand\SubCommands;

use App\DUI\CommandExec;
use App\DUI\DockerCommand\DockerCommand;

class DockerVolumeCommand extends DockerCommand
{
    public static function parseHttpRequestToDockerCommandBuilder($requestData)
    {
        $requestData['subCommand'] = 'volume';

        return parent::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function createVolume($requestData)
    {
        $requestData['subCommandAction'] = 'create';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function removeVolume($requestData)
    {
        $requestData['subCommandAction'] = 'rm';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function listVolumes($requestData)
    {
        $requestData['subCommandAction'] = 'ls';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function inspectVolume($requestData)
    {
        $requestData['subCommandAction'] = 'inspect';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function listFiles($requestData)
    {
        $requestData['subCommandAction'] = 'dummy';
        $instance = static::parseHttpRequestToDockerCommandBuilder($requestData);

        $dockerArguments = $instance->getArguments();

        $command = sprintf(
            'ls -a /var/lib/docker/volumes/%s/_data',
            array_shift($dockerArguments)
        );

        return CommandExec::createInstance()
            ->setCommand($command);
    }
}