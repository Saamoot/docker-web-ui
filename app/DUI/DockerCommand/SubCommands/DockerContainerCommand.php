<?php

namespace App\DUI\DockerCommand\SubCommands;

use App\DUI\DockerCommand\DockerCommand;

class DockerContainerCommand extends DockerCommand
{
    public static function parseHttpRequestToDockerCommandBuilder($requestData)
    {
        $requestData['subCommand'] = 'container';

        return parent::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function runContainer($requestData)
    {
        $requestData['subCommandAction'] = 'run';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function removeContainer($requestData)
    {
        $requestData['subCommandAction'] = 'rm';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function listContainers($requestData)
    {
        $requestData['subCommandAction'] = 'ls';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function inspectContainer($requestData)
    {
        $requestData['subCommandAction'] = 'inspect';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function createContainer($requestData)
    {
        $requestData['subCommandAction'] = 'create';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function stopContainer($requestData)
    {
        $requestData['subCommandAction'] = 'stop';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function restartContainer($requestData)
    {
        $requestData['subCommandAction'] = 'restart';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    public static function startContainer($requestData)
    {
        $requestData['subCommandAction'] = 'start';

        return static::parseHttpRequestToDockerCommandBuilder($requestData);
    }

    /**
     * @param mixed       $value
     * @param string|null $field Available fields: ancestor, before, exited, expose, health, id, is-task, label, name,
     *                           network, publish, since, status, volume
     *
     * @return \App\DUI\DockerCommand\DockerCommandBuilder
     */
    public static function filterContainersBy($value, $field = null)
    {
        if (true === is_null($field)) {
            $field = 'name';
        }

        $requestData['options'] = [
            'all',
            'filter' => sprintf('"%s=%s"', $field, $value),
        ];

        return static::listContainers($requestData);
    }
}
