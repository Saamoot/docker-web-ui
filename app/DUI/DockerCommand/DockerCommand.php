<?php

namespace App\DUI\DockerCommand;

use Exception;

class DockerCommand
{
    static public function sanitizeCommandValue($value)
    {
        if (true === is_array($value)) {
            foreach ($value as $key => $item) {
                $value[$key] = self::sanitizeCommandValue($item);
            }

            return $value;
        }

        return preg_replace('/[^a-zA-Z0-9:\/_\-]*/', '', $value);
    }

    /**
     * @throws \Exception
     */
    static public function parseHttpRequestToDockerCommandBuilder($requestData)
    {
        $dockerCommandBuilder = self::__initDockerCommandBuilder($requestData);

        self::__parseOptions($dockerCommandBuilder, $requestData);
        self::__parseArguments($dockerCommandBuilder, $requestData);

        return $dockerCommandBuilder;
    }

    /**
     * @param array $requestData
     *
     * @return \App\DUI\DockerCommand\DockerCommandBuilder
     * @throws \Exception
     */
    private static function __initDockerCommandBuilder(&$requestData)
    {
        $dockerCommandBuilder = DockerCommandBuilder::getInstance();

        $subCommand       = pull_value_from_array($requestData, 'subCommand', false);
        $subCommandAction = pull_value_from_array($requestData, 'subCommandAction', false);

        if (false === $subCommand || false === $subCommandAction) {
            throw new Exception(sprintf(
                'Invalid request data supplied, missing "%s"',
                '"sub command" or "sub command action"'
            ));
        }

        $dockerCommandBuilder->setSubCommand($subCommand);
        $dockerCommandBuilder->setSubCommandAction($subCommandAction);

        if (true === array_key_exists('containerRuntime', $requestData)) {
            $dockerCommandBuilder->setContainerRuntime($requestData['containerRuntime']);
            unset($requestData['containerRuntime']);
        }

        return $dockerCommandBuilder;
    }

    /**
     * @param \App\DUI\DockerCommand\DockerCommandBuilder $dockerCommandBuilder
     * @param                                             $requestData
     *
     * @return void
     * @throws \Exception
     */
    private static function __parseOptions($dockerCommandBuilder, $requestData)
    {
        $options = pull_value_from_array($requestData, 'options', []);
        foreach ($options as $key => $values) {
            if (true === is_numeric($key)) {
                $sanitizedValue = DockerCommandArguments::sanitizeKey($values);
                $dockerCommandBuilder->addOption('flags', $sanitizedValue);

                continue;
            }

            $sanitizedKey = DockerCommandArguments::sanitizeKey($key);
            if (false === $sanitizedKey) {
                continue;
            }

            if (false === is_array($values)) {
                $values = [$values];
            }

            foreach ($values as $value) {
                $dockerCommandBuilder->addOption($sanitizedKey, $value);
            }
        }
    }

    /**
     * @param \App\DUI\DockerCommand\DockerCommandBuilder $dockerCommandBuilder
     * @param array                                       $requestData
     *
     * @return void
     */
    private static function __parseArguments($dockerCommandBuilder, $requestData)
    {
        $arguments = pull_value_from_array($requestData, 'arguments', []);

        $dockerCommandBuilder->setArguments($arguments);
    }

}