<?php

namespace App\DUI\DockerCommand;

class DockerCommandArguments
{
    static private $__map = [
        'a'           => 'all',
        'all'         => 'all',
        'filter'      => 'filter',
        'format'      => 'format',
        'v'           => 'volume',
        'volume'      => 'volume',
        'u'           => 'user',
        'user'        => 'user',
        'p'           => 'publish',
        'publish'     => 'publish',
        'P'           => 'publish-all',
        'publish-all' => 'publish-all',
        'rm'          => 'rm',
        'd'           => 'detach',
        'detach'      => 'detach',
        'e'           => 'env',
        'env'         => 'env',
        'link'        => 'link',
        't'           => 'tty',
        'tty'         => 'tty',
        'w'           => 'workdir',
        'workdir'     => 'workdir',
        'privileged'  => 'privileged',
        'name'        => 'name',
        'i'           => 'interactive',
        'interactive' => 'interactive',
        'h'           => 'hostname',
        'hostname'    => 'hostname',
        'l'           => 'label',
        'label'       => 'label',
        'restart'     => 'restart',
        'mount'       => 'mount',
        'host'        => 'host',
        'H'           => 'host',
    ];

    static public function sanitizeKey($argument, $fallbackValue = false)
    {
        if (true === array_key_exists($argument, self::$__map)) {
            return self::$__map[$argument];
        }

        return $fallbackValue;
    }
}