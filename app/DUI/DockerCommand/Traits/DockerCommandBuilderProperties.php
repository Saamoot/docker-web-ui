<?php

namespace App\DUI\DockerCommand\Traits;

use Exception;

trait DockerCommandBuilderProperties
{
    private $__containerRuntime          = 'docker';
    private $__subCommand                = 'undefined-sub-command';
    private $__subCommandAction          = 'undefined-sub-command-action';
    private $__options                   = [];
    private $__arguments                 = [];
    private $__containerRuntimeArguments = [];

    /**
     * @return string
     */
    public function getContainerRuntime()
    {
        return $this->__containerRuntime;
    }

    /**
     * @param string $runTime
     *
     * @return $this
     */
    public function setContainerRuntime($runTime)
    {
        $this->__containerRuntime = $runTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubCommand()
    {
        return $this->__subCommand;
    }

    /**
     * @param string $_subCommand
     *
     * @return $this
     */
    public function setSubCommand($_subCommand)
    {
        $this->__subCommand = $_subCommand;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubCommandAction()
    {
        return $this->__subCommandAction;
    }

    /**
     * @param string $_subCommandAction
     *
     * @return $this
     */
    public function setSubCommandAction($_subCommandAction)
    {
        $this->__subCommandAction = $_subCommandAction;

        return $this;
    }

    /**
     * @param string      $name
     * @param string|null $value
     *
     * @return $this
     * @throws \Exception
     */
    public function addOption($name, $value = null, $index = null)
    {
        if (false === array_key_exists($name, $this->__options)) {
            $this->__options[$name] = [];
        }

        if (true === is_numeric($name)) {
            if (false === array_key_exists('flags', $this->__options)) {
                $this->__options['flags'] = [];
            }

            $this->__options['flags'][] = $value;

            return $this;
        }

        $indexType = 'supplied';
        if (true === is_null($index)) {
            $indexType         = 'generated';
            $argumentNameCount = count($this->__options[$name]);
            $index             = $name . $argumentNameCount;
        }

        if (true === array_key_exists($index, $this->__options[$name])) {
            throw new Exception(sprintf(
                'Duplicate index "%s" "%s".',
                $index,
                $indexType
            ));
        }

        $this->__options[$name][$index] = $value;

        return $this;
    }

    public function setOptions($options)
    {
        $this->__options = $options;

        return $this;
    }

    public function hasOptions()
    {
        return false === empty($this->__options);
    }

    public function getOptions($name = null, $fallbackValue = false)
    {
        if (true === is_null($name)) {
            return $this->__options;
        }

        if (true === array_key_exists($name, $this->__options)) {
            return $this->__options[$name];
        }

        return $fallbackValue;
    }

    public function setArguments($arguments)
    {
        $this->__arguments = $arguments;

        return $this;
    }

    public function addArgument($argument)
    {
        $this->__arguments[] = $argument;

        return $this;
    }

    public function getArguments()
    {
        return $this->__arguments;
    }

    public function hasArguments()
    {
        return false === empty($this->__arguments);
    }

    /**
     * @return array
     */
    public function getContainerRuntimeArguments()
    {
        return $this->__containerRuntimeArguments;
    }

    /**
     * @param array $containerRuntimeArguments
     *
     * @return $this
     */
    public function setContainerRuntimeArguments($containerRuntimeArguments)
    {
        $this->__containerRuntimeArguments = $containerRuntimeArguments;

        return $this;
    }

    /**
     * @param string $argumentName
     * @param string $argumentValue
     *
     * @return $this
     */
    public function setContainerRuntimeArgument($argumentName, $argumentValue)
    {
        $this->__containerRuntimeArguments[$argumentName] = $argumentValue;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasContainerRuntimeArguments()
    {
        return false === empty($this->getContainerRuntimeArguments());
    }
}