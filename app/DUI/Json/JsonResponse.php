<?php

namespace App\DUI\Json;

use App\DUI\Json\JsonResponseTraits\LaravelEloquentMethods;
use App\DUI\Json\JsonResponseTraits\NamedSetters;
use App\DUI\Json\JsonResponseTraits\Properties;

class JsonResponse
{
    use Properties;
    use LaravelEloquentMethods;
    use NamedSetters;

    public const RESPONSE_CODE_SUCCESS         = 0;
    public const RESPONSE_CODE_GENERAL_FAILURE = 1;

    /**
     * @return \App\DUI\Json\JsonResponse
     */
    static public function getInstance()
    {
        return new static();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'responseCode' => $this->getResponseCode(),
            'message'      => $this->getMessage(),
            'data'         => $this->getData(),
            'timestamp'    => $this->getTimestamp(),
        ];
    }

    /**
     * @return false|string
     */
    public function toJson()
    {
        return json_encode($this->toArray(), $this->getJsonEncodeFlags());
    }

    /**
     * @param $jsonString
     *
     * @return \App\DUI\Json\JsonResponse
     */
    static public function fromJson($jsonString)
    {
        $data = json_decode($jsonString, true);

        $result = self::getInstance();

        foreach ($result->toArray() as $key => $value) {
            if (false === array_key_exists($key, $data)) {
                continue;
            }

            $method = sprintf('set%s', ucfirst($key));

            call_user_func([$result, $method], $value);
        }

        return $result;
    }
}