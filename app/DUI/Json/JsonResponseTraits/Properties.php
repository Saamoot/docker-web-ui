<?php

namespace App\DUI\Json\JsonResponseTraits;

trait Properties
{
    private $__response_code   = 0;
    private $__message         = '';
    private $__data            = [];
    private $__jsonEncodeFlags = 0;
    private $__timestamp       = null;

    /**
     * @return int
     */
    public function getResponseCode()
    {
        return $this->__response_code;
    }

    /**
     * @param int $responseCode
     *
     * @return $this
     */
    public function setResponseCode($responseCode)
    {
        $this->__response_code = $responseCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->__message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->__message = $message;

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->__data;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->__data = $data;

        return $this;
    }

    public function setDataFromJsonString($data)
    {
        if (true === is_array($data)) {
            $data = '[' . implode(',', $data) . ']';
        }

        return $this->setData(json_decode($data, true));
    }

    /**
     * @param int|string $key
     * @param mixed      $value
     *
     * @return $this
     */
    public function setDataItem($key, $value)
    {
        $this->__data[$key] = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getJsonEncodeFlags()
    {
        return $this->__jsonEncodeFlags;
    }

    /**
     * @param int $jsonEncodeFlags
     *
     * @return $this
     */
    public function setJsonEncodeFlags($jsonEncodeFlags)
    {
        $this->__jsonEncodeFlags = $jsonEncodeFlags;

        return $this;
    }

    public function addPrettyPrintJsonEncodeFlag()
    {
        $this->__jsonEncodeFlags = $this->__jsonEncodeFlags || JSON_PRETTY_PRINT;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        if (true === is_null($this->__timestamp)) {
            $this->__timestamp = date('c');
        }

        return $this->__timestamp;
    }

    /**
     * @param string $timestamp
     *
     * @return $this
     */
    public function setTimestamp($timestamp)
    {
        $this->__timestamp = $timestamp;

        return $this;
    }

}