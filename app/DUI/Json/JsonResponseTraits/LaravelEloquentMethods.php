<?php

namespace App\DUI\Json\JsonResponseTraits;

use App\DUI\Json\JsonResponse;

trait LaravelEloquentMethods
{
    /**
     * @param \Illuminate\Database\Eloquent\Model|\App\DUI\Repository\Repository $model
     *
     * @return \App\DUI\Json\JsonResponse
     */
    static public function fromModelPaginated($model)
    {
        $result = new JsonResponse();

        $result->setData($model->paginate()->toArray());

        return $result;
    }
}