<?php

namespace App\DUI\Json\JsonResponseTraits;

use App\DUI\Json\JsonResponse;

trait NamedSetters
{
    public function setSuccessResponseCode()
    {
        return $this->setResponseCode(JsonResponse::RESPONSE_CODE_SUCCESS);
    }

    public function setGeneralFailureResponseCode()
    {
        return $this->setResponseCode(JsonResponse::RESPONSE_CODE_GENERAL_FAILURE);
    }
}