<?php

namespace App\DUI\Json;

class LaravelJsonResponse
{
    /**
     * @param \App\DUI\Json\JsonResponse $jsonResponse
     * @param int                        $status
     * @param array                      $headers
     * @param int                        $options
     *
     * @return \Illuminate\Http\JsonResponse
     */
    static public function fromDuiJsonResponse($jsonResponse, $status = 200, $headers = [], $options = 0)
    {
        if (false === $jsonResponse instanceof JsonResponse) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid argument supplied, expected instance of "%s", got "%s"',
                JsonResponse::class,
                get_class($jsonResponse)
            ));
        }

        return response()->json($jsonResponse->toArray(), $status, $headers, $options);
    }
}