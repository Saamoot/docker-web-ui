<?php

namespace App\DUI;

use Exception;

class CommandExec
{
    /**
     * @var null|string
     */
    private $__command = null;
    /**
     * @var int
     */
    private $__resultCode = -1;
    /**
     * @var null|string[]]
     */
    private $__output = null;
    /**
     * @var null|string
     */
    private $__lastLine = null;

    /**
     * @var bool
     */
    private $__throwExceptionOnNonZeroCode = true;

    /**
     * @var bool
     */
    private $__redirectStderrToStdout = true;

    /**
     * @return int
     */
    public function getResultCode()
    {
        return $this->__resultCode;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->getResultCode() === 0;
    }

    /**
     * @param int $resultCode
     *
     * @return \App\DUI\CommandExec
     */
    public function setResultCode($resultCode)
    {
        $this->__resultCode = $resultCode;

        return $this;
    }

    /**
     * @return null|string[]
     */
    public function getOutput()
    {
        return $this->__output;
    }

    /**
     * @param string[] $output
     *
     * @return \App\DUI\CommandExec
     */
    public function setOutput($output)
    {
        $this->__output = $output;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLastLine()
    {
        return $this->__lastLine;
    }

    /**
     * @param string $lastLine
     *
     * @return \App\DUI\CommandExec
     */
    public function setLastLine($lastLine)
    {
        $this->__lastLine = $lastLine;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCommand()
    {
        $result = $this->__command;

        if (true === $this->getRedirectStderrToStdoutEnabled()) {
            $result .= ' 2>&1';
        }

        return  $result;
    }

    /**
     * @param string $command
     *
     * @return \App\DUI\CommandExec
     */
    public function setCommand($command)
    {
        $this->__command = $command;

        return $this;
    }

    /**
     * @return \App\DUI\CommandExec
     */
    public static function createInstance()
    {
        return new static();
    }

    /**
     * @return \App\DUI\CommandExec
     */
    public static function executeString($command)
    {
        $instance = self::createInstance();
        $instance->setCommand($command);

        return $instance->execute();
    }

    public function execute()
    {
        $lastLine = exec($this->getCommand(), $output, $resultCode);

        $this->setLastLine($lastLine);
        $this->setOutput($output);
        $this->setResultCode($resultCode);

        if (
            true === $this->getThrowExceptionOnNonZeroCode()
            && $this->getResultCode() > 0
        ) {
            throw new Exception(sprintf(
                'Docker command "%s" failed with error code "%d" and last line "%s"',
                $this->getCommand(),
                $this->getResultCode(),
                $this->getLastLine()
            ));
        }

        return $this;
    }

    public function outputAsJsonString()
    {
        if (true === is_null($this->getOutput())) {
            return false;
        }

        $output = implode(',', $this->getOutput());

        return '[' . $output . ']';
    }

    public function outputAsDecodedJson()
    {
        return json_decode($this->outputAsJsonString(), true);
    }

    /**
     * @return bool
     */
    public function getThrowExceptionOnNonZeroCode()
    {
        return $this->__throwExceptionOnNonZeroCode;
    }

    /**
     * @param bool $throwExceptionOnNonZeroCode
     *
     * @return \App\DUI\CommandExec
     */
    public function setThrowExceptionOnNonZeroCode($throwExceptionOnNonZeroCode)
    {
        $this->__throwExceptionOnNonZeroCode = $throwExceptionOnNonZeroCode;

        return $this;
    }

    /**
     * @return bool
     */
    public function getRedirectStderrToStdoutEnabled()
    {
        return $this->__redirectStderrToStdout;
    }

    /**
     * @param bool $redirectStderrToStdout
     *
     * @return $this
     */
    public function setRedirectStderrToStdout($redirectStderrToStdout = true)
    {
        $this->__redirectStderrToStdout = boolval($redirectStderrToStdout);

        return $this;
    }



}