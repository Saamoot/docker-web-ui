<?php

namespace App\Http\Controllers\Api\V1;

use App\DUI\DockerCommand\SubCommands\DockerVolumeCommand;
use App\DUI\Json\JsonResponse;
use Exception;

class VolumesController extends BaseControllerV1
{
    public function index()
    {
        $requestData          = request()->all();
        $dockerCommandBuilder = DockerVolumeCommand::listVolumes($requestData);
        $commandExecInstance  = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        return JsonResponse::getInstance()
            ->setDataFromJsonString($commandExecInstance->getOutput())
            ->setMessage('List of volumes')
            ->setResponseCode($commandExecInstance->getResultCode())
            ->addPrettyPrintJsonEncodeFlag()
            ->toJson();
    }

    public function list()
    {
        return $this->index();
    }

    public function show($volume = null)
    {
        $requestData                          = request()->all();
        $requestData['arguments']['volume'] = request()->input('volume', $volume);
        $dockerCommandBuilder                 = DockerVolumeCommand::inspectVolume($requestData);
        $commandExecInstance                  = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        if (0 !== $commandExecInstance->getResultCode()) {
            throw new Exception(sprintf(
                'Unexpected error occurred, got response code "%d"',
                $commandExecInstance->getResultCode()
            ));
        }

        return JsonResponse::getInstance()
            ->setDataFromJsonString($commandExecInstance->getOutput()[0])
            ->setResponseCode($commandExecInstance->getResultCode())
            ->setMessage(__('Inspect of \':volume\' volume', ['volume' => $requestData['arguments']['volume']]))
            ->toJson();
    }

    public function inspect($volume = null)
    {
        return $this->show($volume);
    }

    public function store($volume = null)
    {
        $requestData                          = request()->all();
        $requestData['arguments']['volume'] = request()->input('volume', $volume);
        $dockerCommandBuilder                 = DockerVolumeCommand::createVolume($requestData);
        $commandExecInstance                  = $dockerCommandBuilder
            ->execute();

        $createdVolumeName = null;
        if ($commandExecInstance->isSuccessful()) {
            $createdVolumeName = $commandExecInstance->getOutput()[0];
        }

        return JsonResponse::getInstance()
            ->setData([
                'volume_name' => $createdVolumeName,
            ])
            ->setMessage(__('Volume \':volume\' created', ['volume' => $createdVolumeName]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function create($volume = null)
    {
        return $this->store($volume);
    }

    public function destroy($volume = null)
    {
        $requestData                          = request()->all();
        $requestData['arguments']['volume'] = request()->input('volume', $volume);
        $dockerCommandBuilder                 = DockerVolumeCommand::removeVolume($requestData);
        $commandExecInstance                  = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('Volume \':volume\' deleted', ['volume' => $requestData['arguments']['volume']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function remove($volume = null)
    {
        return $this->destroy($volume);
    }

    public function files($volume = null)
    {
        $requestData                          = request()->all();
        $requestData['arguments']['volume'] = request()->input('volume', $volume);

        $dockerCommandBuilder = DockerVolumeCommand::listFiles($requestData);
        $commandExecInstance  = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('List of files on volume \':volume\' ', ['volume' => $requestData['arguments']['volume']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }
}