<?php

namespace App\Http\Controllers\Api\V1;

use App\DUI\DockerCommand\SubCommands\DockerNetworkCommand;
use App\DUI\Json\JsonResponse;

class NetworksController extends BaseControllerV1
{
    public function index()
    {
        $requestData               = request()->all();
        $dockerCommandBuilder      = DockerNetworkCommand::listNetworks($requestData);
        $dockerCommandExecInstance = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        $this->execCommandErrorResponse($dockerCommandExecInstance);

        return JsonResponse::getInstance()
            ->setDataFromJsonString($dockerCommandExecInstance->getOutput())
            ->setMessage('List of networks')
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->addPrettyPrintJsonEncodeFlag()
            ->toJson();
    }

    public function list()
    {
        return $this->index();
    }

    public function show($network = null)
    {
        $requestData                         = request()->all();
        $requestData['arguments']['network'] = request()->input('network', $network);
        $dockerCommandBuilder                = DockerNetworkCommand::inspectNetwork($requestData);
        $dockerCommandExecInstance           = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        $this->execCommandErrorResponse($dockerCommandExecInstance);

        return JsonResponse::getInstance()
            ->setDataFromJsonString($dockerCommandExecInstance->getOutput()[0])
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->setMessage(__('Inspect of \':network\' network',
                ['network' => $requestData['arguments']['network']]))
            ->toJson();
    }

    public function inspect($network = null)
    {
        return $this->show($network);
    }

    public function store($network = null)
    {
        $requestData                         = request()->all();
        $requestData['arguments']['network'] = request()->input('network', $network);
        $dockerCommandBuilder                = DockerNetworkCommand::createNetwork($requestData);
        $dockerCommandExecInstance           = $dockerCommandBuilder
            ->execute();

        $this->execCommandErrorResponse($dockerCommandExecInstance);

        return JsonResponse::getInstance()
            ->setData($dockerCommandExecInstance->getOutput())
            ->setMessage(__('Network \':network\' created', ['network' => $requestData['arguments']['network']]))
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->toJson();
    }

    public function create($network = null)
    {
        return $this->store($network);
    }

    public function connect($network = null, $container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['network']   = request()->input('network', $network);
        $requestData['arguments']['container'] = request()->input('container', $container);

        if (true === DockerNetworkCommand::isContainerInNetwork($requestData)) {
            return JsonResponse::getInstance()
                ->setMessage(__(
                    'Container \':container\' is already in network \':network\'',
                    [
                        'container' => $requestData['arguments']['container'],
                        'network'   => $requestData['arguments']['network'],
                    ]
                ))
                ->setResponseCode(0)
                ->toJson();
        }

        $dockerCommandBuilder      = DockerNetworkCommand::connect($requestData);
        $dockerCommandExecInstance = $dockerCommandBuilder
            ->execute();

        $this->execCommandErrorResponse($dockerCommandExecInstance);

        return JsonResponse::getInstance()
            ->setData($dockerCommandExecInstance->getOutput())
            ->setMessage(__(
                'Container \':container\' connected to network \':network\'',
                [
                    'network'   => $requestData['arguments']['network'],
                    'container' => $requestData['arguments']['container'],
                ]
            ))
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->toJson();
    }

    public function disconnect($network = null, $container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['network']   = request()->input('network', $network);
        $requestData['arguments']['container'] = request()->input('container', $container);
        $dockerCommandBuilder                  = DockerNetworkCommand::disconnect($requestData);
        $dockerCommandExecInstance             = $dockerCommandBuilder
            ->execute();

        $this->execCommandErrorResponse($dockerCommandExecInstance);

        return JsonResponse::getInstance()
            ->setData($dockerCommandExecInstance->getOutput())
            ->setMessage(__(
                'Container \':container\' disconnected to network \':network\'',
                [
                    'network'   => $requestData['arguments']['network'],
                    'container' => $requestData['arguments']['container'],
                ]
            ))
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->toJson();
    }

    public function destroy($network = null)
    {
        $requestData                         = request()->all();
        $requestData['arguments']['network'] = request()->input('network', $network);
        $dockerCommandBuilder                = DockerNetworkCommand::removeNetwork($requestData);
        $dockerCommandExecInstance           = $dockerCommandBuilder
            ->execute();

        $this->execCommandErrorResponse($dockerCommandExecInstance);

        return JsonResponse::getInstance()
            ->setData($dockerCommandExecInstance->getOutput())
            ->setMessage(__('Network :network removed', ['network' => $requestData['arguments']['network']]))
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->toJson();
    }

    public function remove($network = null)
    {
        return $this->destroy($network);
    }
}