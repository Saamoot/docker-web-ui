<?php

namespace App\Http\Controllers\Api\V1;

use App\DUI\DockerCommand\SubCommands\DockerContainerCommand;
use App\DUI\Json\JsonResponse;
use Exception;

class ContainersController extends BaseControllerV1
{
    public function index()
    {
        $requestData          = request()->all();
        $dockerCommandBuilder = DockerContainerCommand::listContainers($requestData);
        $commandExecInstance  = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        return JsonResponse::getInstance()
            ->setDataFromJsonString($commandExecInstance->getOutput())
            ->setMessage('List of containers')
            ->setResponseCode($commandExecInstance->getResultCode())
            ->addPrettyPrintJsonEncodeFlag()
            ->toJson();
    }

    public function list()
    {
        return $this->index();
    }

    public function show($container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['container'] = request()->input('container', $container);
        $dockerCommandBuilder                  = DockerContainerCommand::inspectContainer($requestData);
        $commandExecInstance                   = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        if (0 !== $commandExecInstance->getResultCode()) {
            throw new Exception(sprintf(
                'Unexpected error occurred, got response code "%d"',
                $commandExecInstance->getResultCode()
            ));
        }

        return JsonResponse::getInstance()
            ->setDataFromJsonString($commandExecInstance->getOutput()[0])
            ->setResponseCode($commandExecInstance->getResultCode())
            ->setMessage(__('Inspect of \':container\' container',
                ['container' => $requestData['arguments']['container']]))
            ->toJson();
    }

    public function inspect($container = null)
    {
        return $this->show($container);
    }

    public function store($image = null, $container = null)
    {
        $requestData                       = request()->all();
        $requestData['arguments']['image'] = request()->input('image', $image);
        $requestData['options']['name']    = request()->input('container', $container);
        $dockerCommandBuilder              = DockerContainerCommand::createContainer($requestData);
        $commandExecInstance               = $dockerCommandBuilder
            ->execute();

        $createdContainerName = null;
        if ($commandExecInstance->isSuccessful()) {
            $createdContainerName = $commandExecInstance->getOutput()[0];
        }

        $dockerNameExecInstance = DockerContainerCommand::filterContainersBy($createdContainerName, 'id')
            ->addOption('format', '"{{.Names}}"')
            ->execute();

        return JsonResponse::getInstance()
            ->setData([
                'container_name' => $dockerNameExecInstance->getLastLine(),
            ])
            ->setMessage(__('Container \':container\' created', ['container' => $dockerNameExecInstance->getLastLine()]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function create($container = null)
    {
        return $this->store($container);
    }

    public function destroy($container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['container'] = request()->input('container', $container);
        $dockerCommandBuilder                  = DockerContainerCommand::removeContainer($requestData);
        $commandExecInstance                   = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('Container \':container\' deleted',
                ['container' => $requestData['arguments']['container']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function remove($container = null)
    {
        return $this->destroy($container);
    }

    public function stop($container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['container'] = request()->input('container', $container);
        $dockerCommandBuilder                  = DockerContainerCommand::stopContainer($requestData);
        $commandExecInstance                   = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('Container \':container\' stopped',
                ['container' => $requestData['arguments']['container']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function start($container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['container'] = request()->input('container', $container);
        $dockerCommandBuilder                  = DockerContainerCommand::startContainer($requestData);
        $commandExecInstance                   = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('Container \':container\' started',
                ['container' => $requestData['arguments']['container']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function restart($container = null)
    {
        $requestData                           = request()->all();
        $requestData['arguments']['container'] = request()->input('container', $container);
        $dockerCommandBuilder                  = DockerContainerCommand::restartContainer($requestData);
        $commandExecInstance                   = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('Container \':container\' restarted',
                ['container' => $requestData['arguments']['container']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

    public function run($image = null, $container = null)
    {
        $requestData                       = request()->all();
        $requestData['arguments']['image'] = request()->input('image', $image);
        $requestData['options']['name']    = request()->input('container', $container);
        $dockerCommandBuilder              = DockerContainerCommand::runContainer($requestData);
        $commandExecInstance               = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($commandExecInstance->getOutput())
            ->setMessage(__('Container \':container\' started',
                ['container' => $requestData['arguments']['container']]))
            ->setResponseCode($commandExecInstance->getResultCode())
            ->toJson();
    }

}