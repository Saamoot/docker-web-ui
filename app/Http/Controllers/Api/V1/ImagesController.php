<?php

namespace App\Http\Controllers\Api\V1;

use App\DUI\DockerCommand\SubCommands\DockerImageCommand;
use App\DUI\Json\JsonResponse;
use Exception;

class ImagesController extends BaseControllerV1
{
    public function index()
    {
        $requestData               = request()->all();
        $dockerCommandBuilder      = DockerImageCommand::listImages($requestData);
        $dockerCommandExecInstance = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        return JsonResponse::getInstance()
            ->setDataFromJsonString($dockerCommandExecInstance->getOutput())
            ->setMessage('List of images')
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->addPrettyPrintJsonEncodeFlag()
            ->toJson();
    }

    public function list()
    {
        return $this->index();
    }

    public function show($image)
    {
        $requestData                       = request()->all();
        $requestData['arguments']['image'] = request()->input('image', $image);
        $dockerCommandBuilder              = DockerImageCommand::inspectImage($requestData);
        $dockerCommandExecInstance         = $dockerCommandBuilder
            ->setJsonAsOutputFormat()
            ->execute();

        if (0 !== $dockerCommandExecInstance->getResultCode()) {
            throw new Exception(sprintf(
                'Unexpected error occurred, got response code "%d"',
                $dockerCommandExecInstance->getResultCode()
            ));
        }

        return JsonResponse::getInstance()
            ->setDataFromJsonString($dockerCommandExecInstance->getOutput()[0])
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->setMessage(__('Inspect of \':image\' image', ['image' => $requestData['arguments']['image']]))
            ->toJson();
    }

    public function inspect($image)
    {
        return $this->show($image);
    }

    public function store($image = null)
    {
        $requestData                       = request()->all();
        $requestData['arguments']['image'] = request()->input('image', $image);
        $dockerCommandBuilder              = DockerImageCommand::pullImage($requestData);
        $dockerCommandExecInstance         = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($dockerCommandExecInstance->getOutput())
            ->setMessage(__('Image \':image\' stored', ['image' => $requestData['arguments']['image']]))
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->toJson();
    }

    public function pull()
    {
        return $this->store();
    }

    public function destroy($image = null)
    {
        $requestData                       = request()->all();
        $requestData['arguments']['image'] = request()->input('image', $image);
        $dockerCommandBuilder              = DockerImageCommand::removeImage($requestData);
        $dockerCommandExecInstance         = $dockerCommandBuilder
            ->execute();

        return JsonResponse::getInstance()
            ->setData($dockerCommandExecInstance->getOutput())
            ->setMessage(__('Image \':image\' deleted', ['image' => $requestData['arguments']['image']]))
            ->setResponseCode($dockerCommandExecInstance->getResultCode())
            ->toJson();
    }

    public function remove($image)
    {
        return $this->destroy($image);
    }

}