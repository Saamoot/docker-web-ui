<?php

namespace App\Http\Controllers\Api\V1;

use Exception;

abstract class BaseControllerV1
{
    /**
     * @param \App\DUI\CommandExec $dockerCommandExecInstance
     *
     * @return void
     * @throws \Exception
     */
    protected function execCommandErrorResponse($dockerCommandExecInstance)
    {
        if (0 === $dockerCommandExecInstance->getResultCode()) {
            return;
        }

        throw new Exception(sprintf(
            'Unexpected error occurred, got response code "%d"',
            $dockerCommandExecInstance->getResultCode()
        ));
    }
}