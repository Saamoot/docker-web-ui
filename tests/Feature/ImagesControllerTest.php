<?php

namespace Tests\Feature;

use Tests\TestCase;

class ImagesControllerTest extends TestCase
{
    public function testCreateImage()
    {
        $imageName   = 'alpine';
        $imageTag    = 'latest';
        $image     = sprintf('%s:%s', $imageName, $imageTag);
        $requestData = [
            'image' => $image
        ];

        $this->postJson(route('api.v1.images.store'), $requestData)
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => sprintf('%s: Pulling from library/%s', $imageTag, $imageName),
            ]);

        $this->deleteJson(route('api.v1.images.destroy', $requestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => sprintf('Untagged: %s:%s', $imageName, $imageTag)
            ]);
    }

    public function testPullImage()
    {
        $imageName   = 'alpine';
        $imageTag    = 'latest';
        $image     = sprintf('%s:%s', $imageName, $imageTag);
        $requestData = [
            'image' => $image
        ];

        $this->postJson(route('api.v1.images.pull'), $requestData)
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => sprintf('%s: Pulling from library/%s', $imageTag, $imageName),
            ]);

        $this->deleteJson(route('api.v1.images.destroy', $requestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => sprintf('Untagged: %s:%s', $imageName, $imageTag)
            ]);
    }

    public function testCreateAndInspectImage()
    {
        $image     = 'alpine:latest';
        $requestData = [
            'image' => $image
        ];

        $this->postJson(route('api.v1.images.store'), $requestData)
            ->assertStatus(200);

        $this->getJson(route('api.v1.images.show', $image))
            ->assertStatus(200)
            ->assertJsonFragment([
                "RepoTags" => [
                    $image
                ],
            ]);

        $this->deleteJson(route('api.v1.images.destroy', $requestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => sprintf('Untagged: %s', $image)
            ]);
    }

    public function testCreateAndDeleteImage()
    {
        $image     = 'alpine:latest';
        $requestData = [
            'image' => $image
        ];

        $this->postJson(route('api.v1.images.store'), $requestData)
            ->assertStatus(200);

        $this->deleteJson(route('api.v1.images.remove', $image))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => sprintf('Untagged: %s', $image)
            ]);
    }

    public function testListImages()
    {
        $images = [
            ['image' => 'alpine', 'tag' => '3.16'],
            ['image' => 'alpine', 'tag' => '3.15'],
            ['image' => 'alpine', 'tag' => '3.14'],
        ];

        foreach ($images as $imageParts) {
            $requestData = [
                'image' => implode(':', $imageParts),
            ];

            $this->postJson(route('api.v1.images.store'), $requestData)
                ->assertStatus(200);
        }

        $response = $this->getJson(route('api.v1.images.index'))
            ->assertStatus(200);

        foreach ($images as $imageParts) {
            $response
                ->assertJsonFragment([
                    'Repository' => $imageParts['image'],
                ])
                ->assertJsonFragment([
                    'Tag' => $imageParts['tag'],
                ]);

            $image = implode(':', $imageParts);
            $this->deleteJson(route('api.v1.images.destroy', $image))
                ->assertStatus(200);
        }
    }
}