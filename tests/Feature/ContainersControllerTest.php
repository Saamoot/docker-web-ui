<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContainersControllerTest extends TestCase
{
    use WithFaker;

    public function testCreateContainer()
    {
        $container   = $this->faker->word() . $this->faker->word();
        $requestData = [
            'container' => $container,
            'image'     => 'alpine:latest',
        ];

        $this->postJson(route('api.v1.containers.store'), $requestData)
            ->assertStatus(200)
            ->assertJsonFragment([
                'container_name' => $container,
            ])
            ->assertJsonFragment([
                'message' => __('Container \':container\' created', ['container' => $container])
            ]);

        $this->deleteJson(route('api.v1.containers.destroy', $container));
    }

    public function testCreateAndInspectContainer()
    {
        $container   = $this->faker->word() . $this->faker->word();
        $requestData = [
            'container' => $container,
            'image'     => 'alpine:latest',
        ];

        $this->postJson(route('api.v1.containers.store'), $requestData)
            ->assertStatus(200);

        $this->getJson(route('api.v1.containers.store', $container))
            ->assertStatus(200)
            ->assertJsonFragment([
                'Name' => '/' . $container,
            ])
            ->assertJsonFragment([
                'Image' => $requestData['image'],
            ]);

        $this->deleteJson(route('api.v1.containers.remove', $container));
    }

    public function testCreateAndDeleteContainer()
    {
        $container   = $this->faker->word() . $this->faker->word();
        $requestData = [
            'container' => $container,
            'image'     => 'alpine:latest',
        ];

        $this->postJson(route('api.v1.containers.store', $requestData))
            ->assertStatus(200);

        $this->deleteJson(route('api.v1.containers.remove', $container))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => $container,
            ])
            ->assertJsonFragment([
                'message' => __('Container \':container\' deleted', ['container' => $container]),
            ]);
    }

    public function testListingOfContainers()
    {
        $imageName = 'alpine:latest';
        $containers = [
            $this->faker->word() . $this->faker->word(),
            $this->faker->word() . $this->faker->word(),
            $this->faker->word() . $this->faker->word(),
        ];

        foreach ($containers as $container) {
            $requestData = [
                'container' => $container,
                'image'     => $imageName,
            ];

            $this->postJson(route('api.v1.containers.store'), $requestData)
                ->assertStatus(200);
        }

        $response = $this->getJson(route('api.v1.containers.index', ['options' => ['all']]))
            ->assertStatus(200);

        foreach ($containers as $container) {
            $response
                ->assertJsonFragment([
                    'Names' => $container,
                ]);

            $this->deleteJson(route('api.v1.containers.destroy', $container));
        }
    }
}