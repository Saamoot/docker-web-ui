<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VolumesControllerTest extends TestCase
{
    use WithFaker;

    public function testCreateNamelessVolume()
    {
        $this->postJson(route('api.v1.volumes.store'))
            ->assertStatus(200)
            ->assertSee([
                'volume_name',
                'created',
            ]);
    }

    public function testCreateNamedVolume()
    {
        $volume    = $this->faker->word() . $this->faker->word();
        $requestData = [
            'volume' => $volume,
        ];

        $this->postJson(route('api.v1.volumes.store'), $requestData)
            ->assertStatus(200)
            ->assertJsonFragment([
                'volume_name' => $volume,
            ])
            ->assertJsonFragment([
                'message' => __('Volume \':volume\' created', ['volume' => $volume])
            ]);
    }

    public function testCreateAndInspectVolume()
    {
        $volume    = $this->faker->word() . $this->faker->word();
        $requestData = [
            'volume' => $volume,
        ];

        $this->postJson(route('api.v1.volumes.store'), $requestData)
            ->assertStatus(200);

        $this->getJson(route('api.v1.volumes.store', $volume))
            ->assertStatus(200)
            ->assertJsonFragment([
                'Name' => $volume,
            ])
            ->assertJsonFragment([
                'Mountpoint' => sprintf('/var/lib/docker/volumes/%s/_data', $volume),
            ])
            ->assertJsonFragment([
                'Scope' => 'local',
            ])
            ->assertJsonFragment([
                'Driver' => 'local',
            ]);
    }

    public function testCreateAndDeleteVolume()
    {
        $volume    = $this->faker->word() . $this->faker->word();
        $requestData = [
            'volume' => $volume,
        ];

        $this->postJson(route('api.v1.volumes.store', $requestData))
            ->assertStatus(200);

        $this->deleteJson(route('api.v1.volumes.remove', $volume))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => $volume,
            ])
            ->assertJsonFragment([
                'message' => __('Volume \':volume\' deleted', ['volume' => $volume]),
            ]);
    }

    public function testListingOfVolumes()
    {
        $volumes = [
            $this->faker->word() . $this->faker->word(),
            $this->faker->word() . $this->faker->word(),
            $this->faker->word() . $this->faker->word(),
        ];

        foreach ($volumes as $volume) {
            $requestData = [
                'volume' => $volume,
            ];

            $this->postJson(route('api.v1.volumes.store'), $requestData)
                ->assertStatus(200);
        }

        $response = $this->getJson(route('api.v1.volumes.index'))
            ->assertStatus(200);

        foreach ($volumes as $volume) {
            $response
                ->assertJsonFragment([
                    'Name' => $volume,
                ])
                ->assertJsonFragment([
                    'Mountpoint' => sprintf('/var/lib/docker/volumes/%s/_data', $volume),
                ])
                ->assertJsonFragment([
                    'Driver' => 'local',
                ])
                ->assertJsonFragment([
                    'Scope' => 'local',
                ]);

            $this->deleteJson(route('api.v1.volumes.destroy', $volume));
        }
    }

    public function testListVolumeFiles()
    {
        $volume    = $this->faker->word() . $this->faker->word();
        $requestData = [
            'volume' => $volume,
        ];

        $response = $this->postJson(route('api.v1.volumes.store', $requestData))
            ->assertStatus(200);

        $volumeName = $response->json('data.volume_name');

        $this->getJson(route('api.v1.volumes.files', $volumeName))
            ->assertStatus(200)
            ->assertJsonFragment([
                '.',
            ])
            ->assertJsonFragment([
                '..',
            ]);
    }
}