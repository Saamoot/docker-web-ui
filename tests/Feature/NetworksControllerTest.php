<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NetworksControllerTest extends TestCase
{
    use WithFaker;

    public function testCreateNetwork()
    {
        $network     = $this->faker->word() . $this->faker->word();
        $requestData = [
            'network' => $network
        ];

        $this->postJson(route('api.v1.networks.store', $requestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => __('Network \':network\' created', ['network' => $network])
            ]);
    }

    public function testCreateAndInspectNetwork()
    {
        $network     = $this->faker->word() . $this->faker->word();
        $requestData = [
            'network' => $network
        ];

        $this->postJson(route('api.v1.networks.store', $requestData))
            ->assertStatus(200);

        $this->getJson(route('api.v1.networks.show', $network))
            ->assertStatus(200)
            ->assertJsonFragment([
                'Name' => $network,
            ]);
    }

    public function testCreateAndDeleteNetwork()
    {
        $network     = $this->faker->word() . $this->faker->word();
        $requestData = [
            'network' => $network
        ];

        $this->postJson(route('api.v1.networks.store', $requestData))
            ->assertStatus(200);

        $this->deleteJson(route('api.v1.networks.remove', $network))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => $network,
            ]);
    }

    public function testConnectDisconnectContainerToNetwork()
    {
        $container                   = 'dui-web';
        $network                     = $this->faker->word() . $this->faker->word();
        $networkRequestData          = ['network' => $network];
        $containerNetworkRequestData = ['network' => $network, 'container' => $container];

        $this->postJson(route('api.v1.networks.store'), $networkRequestData)
            ->assertStatus(200);

        $this->postJson(route('api.v1.networks.connect', $containerNetworkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => __(
                    'Container \':container\' connected to network \':network\'',
                    $containerNetworkRequestData
                )
            ]);

        $this->patchJson(route('api.v1.networks.disconnect', $containerNetworkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => __(
                    'Container \':container\' disconnected to network \':network\'',
                    $containerNetworkRequestData
                )
            ]);

        $this->deleteJson(route('api.v1.networks.remove', $networkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => $network,
            ]);
    }

    public function testConnectContainerToNetworkMultipleTimesAndDisconnect()
    {
        $container                   = 'dui-web';
        $network                     = $this->faker->word() . $this->faker->word();
        $networkRequestData          = ['network' => $network];
        $containerNetworkRequestData = ['network' => $network, 'container' => $container];

        $this->postJson(route('api.v1.networks.store'), $networkRequestData)
            ->assertStatus(200);

        $this->postJson(route('api.v1.networks.connect', $containerNetworkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => __(
                    'Container \':container\' connected to network \':network\'',
                    $containerNetworkRequestData
                )
            ]);

        $this->postJson(route('api.v1.networks.connect', $containerNetworkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => __(
                    'Container \':container\' is already in network \':network\'',
                    $containerNetworkRequestData
                )
            ]);


        $this->patchJson(route('api.v1.networks.disconnect', $containerNetworkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                'message' => __(
                    'Container \':container\' disconnected to network \':network\'',
                    $containerNetworkRequestData
                )
            ]);

        $this->deleteJson(route('api.v1.networks.remove', $networkRequestData))
            ->assertStatus(200)
            ->assertJsonFragment([
                0 => $network,
            ]);
    }

    public function testListNetworks()
    {
        $networks = [
            $this->faker->word() . $this->faker->word(),
            $this->faker->word() . $this->faker->word(),
            $this->faker->word() . $this->faker->word(),
        ];

        foreach ($networks as $network) {
            $requestData = [
                'network' => $network
            ];

            $this->postJson(route('api.v1.networks.store', $requestData))
                ->assertStatus(200);
        }

        $response = $this->getJson(route('api.v1.networks.index'))
            ->assertStatus(200);

        foreach ($networks as $network) {
            $response
                ->assertJsonFragment([
                    'Name' => $network,
                ])
                ->assertJsonFragment([
                    'Scope' => 'local',
                ]);

            $this->deleteJson(route('api.v1.networks.destroy', $network));
        }
    }
}