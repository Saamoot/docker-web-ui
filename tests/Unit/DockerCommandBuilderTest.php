<?php

namespace Tests\Unit;

use App\DUI\DockerCommand\DockerCommandBuilder;
use Tests\TestCase;

class DockerCommandBuilderTest extends TestCase
{
    public function testBuildCommand()
    {
        $instance = new DockerCommandBuilder();

        $instance->setSubCommand('container');
        $instance->setSubCommandAction('run');
        $instance
            ->addOption('e', 'FOO=value1')
            ->addOption('e', 'BAR=value2')
            ->addOption('p', '80:80')
            ->addOption('p', '443:443')
            ->addOption('i')
            ->addOption('t')
            ->addOption('rm')
            ->addArgument('alpine:latest')
            ->addArgument('ls /');

        $this->assertEquals(
            'docker container run --env FOO=value1 --env BAR=value2 --publish 80:80 --publish 443:443 --interactive --tty --rm alpine:latest ls /',
            $instance->build()
        );
    }
}