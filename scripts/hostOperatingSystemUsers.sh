#!/usr/bin/env sh

dataDir="/data/projects/docker-web-ui/storage/app/users"
output="/dev/stdout"

#
# Functions
#

getUserFiles() {
  userFilesDir="${1}"

  echo "$(ls "${userFilesDir}" | grep ".json")"
}

getUserMd5CheckSumFilename() {
  userHomeDir="${1}"

  echo "${userHomeDir}/.docker-web-ui-checksum.md5"
}

getUserMd5CheckSum() {
  userHomeDir="${1}"
  checkSumFile="$(getUserMd5CheckSumFilename "${userHomeDir}")"

  oldCheckSum=""
  if [ -f "${checkSumFile}" ]; then
    oldCheckSum="$(cat "${checkSumFile}")"
  fi

  echo "${oldCheckSum}"
}

getUserCurrentMd5CheckSum() {
  userHomeDir="${1}"

  echo md5sum "${userHomeDir}"
}

setUserMd5CheckSum() {
  username="${1}"
  userHomeDir="${2}"

  newCheckSum="$(getUserCurrentMd5CheckSum "${userHomeDir}")"
  checkSumFile="$(getUserMd5CheckSumFilename "${userHomeDir}")"
  echo "${newCheckSum}" > "${checkSumFile}"
  chown "${username}":"${username}" "${checkSumFile}"
  chmod 600 "${checkSumFile}"
}

getUserShell() {
    isChRootedSftpAllowed="${1:-false}"
    shell="/usr/sbin/nologin"

    if [ "true" = "${isChRootedSftpAllowed}" ]; then
      shell="/bin/bash"
    fi

    echo "${shell}"
}

userExist() {
  id -u "${username}" >/dev/null 2>&1

  if [ "1" = "${?}" ]; then
    echo "false"
    return 1
  fi

  echo "true"
  return 0
}

getUserCommand() {
  username="${1}"

  userCommand="usermod"
  id -u "${username}" >/dev/null 2>&1
  if [ "false" = "$(userExist "${username}")" ]; then
    userCommand="useradd --create-home"
  fi

  echo "${userCommand}"
}

archiveFile() {
  archiveDir="${1}"
  fileToArchive="${2}"
  fileToArchivePostfix="${3}"

  if [ -f "${fileToArchive}" ]; then
    mkdir -p "${archiveDir}"
    mv "${fileToArchive}" "${archiveDir}/$(basename "${fileToArchive}")_${fileToArchivePostfix}"
  fi
}

disableUser() {
  username=${1}

  if [ "false" = "$(userExist "${username}")" ]; then
    return 0
  fi

  usermod -L "${username}"
  passwd -l "${username}"
  usermod -s /sbin/nologin "${username}"
}

skipUserConfigBasedOnMd5CheckSum() {
  username="${1}"
  userHomeDir="/home/${username}"
  oldCheckSum="$(getUserMd5CheckSum "${userHomeDir}")"


  if [ ! -d "${userHomeDir}" ]; then
    echo "false"
    return 1
  fi

  if [ "" = "${oldCheckSum}" ]; then
    echo "false"
    return 2
  fi

  if [ "${oldCheckSum}" != "${newCheckSum}" ]; then
    echo "false"
    return 3
  fi

  echo "true"
  return 0
}

processUserSshConfiguration() {
  username="${1}"
  userHomeDir="${2:-}"
  if [ -z "${userHomerDir}" ]; then
    userHomeDir="/home/${username}"
  fi

  sshDir="${userHomeDir}/.ssh"
  sshArchiveDir="${sshDir}/archive"
  mkdir -p "${sshArchiveDir}"
  sshAuthorizedKeysFile="${sshDir}/authorized_keys"
  sshConfigFile="${sshDir}/config"

  date="$(date +"%Y-%m-%d_%H-%M-%S")"
  archiveFile "${sshArchiveDir}" "${sshAuthorizedKeysFile}" "${date}"
  archiveFile "${sshArchiveDir}" "${sshConfigFile}" "${date}"

  #  echo "username: ${username}"
  #  echo "enabled: ${enabled}"
  #  echo "isChRootedSftpAllowed: ${isChRootedSftpAllowed}"
  #  echo "ssh public keys data: ${sshPublicKeysData}"
  #  echo "ssh configHosts data: ${sshConfigHostData}"

  sshPublicKeysData="$(echo "${sshPublicKeysData}" | jq -r '.[] | @base64')"
  for publicKey in ${sshPublicKeysData}; do
    publicKey="$(echo "${publicKey}" | base64 --decode)"

    publicKeyEnabled="$(echo "${publicKey}" | jq '.enabled')"

    if [ "false" = "${publicKeyEnabled}" ]; then
      continue
    fi

    publicKeyValue="$(echo "${publicKey}" | jq '.publicKey')"
    echo "${publicKeyValue}" >>"${sshAuthorizedKeysFile}"
    #echo "publicKey enabled: ${publicKeyEnabled}"
    #echo "publicKey value: ${publicKeyValue}"
  done

  sshConfigHostData="$(echo "${sshConfigHostData}" | jq -r '.[] | @base64')"
  for configHost in ${sshConfigHostData}; do
    configHost="$(echo "${configHost}" | base64 --decode)"

    configHostEnabled="$(echo "${configHost}" | jq '.enabled')"

    if [ "false" = "${configHostEnabled}" ]; then
      continue
    fi

    configHostIdentifier="$(echo "${configHost}" | jq '.identifier')"
    configHostHosts="$(echo "${configHost}" | jq -r '.hosts[]')"
    configHostHosts="$(echo "${configHostHosts}" | sed ':a;N;$!ba;s/\n/ /g')"
    configHostOptions="$(echo "${configHost}" | jq -r '.options[] | @base64')"

    echo "Host ${configHostHosts}" >>"${sshConfigFile}"

#    echo "---"
#    echo "configHostIdentifier: ${configHostIdentifier}"
#    echo "configHostEnabled: ${configHostEnabled}"
#    echo "configHostHosts: ${configHostHosts}"
    for configHostOption in ${configHostOptions}; do
      configHostOption="$(echo "${configHostOption}" | base64 --decode)"
      configHostOptionName="$(echo "${configHostOption}" | jq -r '.name')"
      configHostOptionValue="$(echo "${configHostOption}" | jq -r '.value')"
      printf "\t%s %s" "${configHostOptionName}" "${configHostOptionValue}" >>"${sshConfigFile}"
#      echo "configHostOptionName: ${configHostOptionName}"
#      echo "configHostOptionValue: ${configHostOptionValue}"
    done
    echo "" >>"${sshConfigFile}"
#    echo "---"
  done

  chown -R "${username}:${username}" "${userHomeDir}"
  chmod 600 "${sshConfigFile}" "${sshAuthorizedKeysFile}"

  chmod 700 "${sshArchiveDir}"
  if [ 1 -lt "$(ls -l "${sshArchiveDir}" | wc -l)" ]; then
    chmod 600 "${sshArchiveDir}/*"
  fi
}

processUserFile() {
  dataDir="${1}"
  userFile="${2}"
  filename="${dataDir}/${userFile}"

  enabled="$(jq '.enabled' "${filename}")"
  username="$(jq -r '.name' "${filename}")"
  userHomeDir="/home/${username}"

  if [ "false" = "${enabled}" ]; then
    disableUser "${username}"
    return 0
  fi

  if [ "true" = "$(skipUserConfigBasedOnMd5CheckSum "${username}")" ]; then
    echo "skipping \"${username}\", no change detected" >> "${output}"
    return 0
  fi

  isChRootedSftpAllowed="$(jq -r '.isChRootedSftpAllowed' "${filename}")"
  sshConfigHostData="$(jq '.configHosts' "${filename}")"
  sshPublicKeysData="$(jq '.keys' "${filename}")"
  shell="$(getUserShell "${isChRootedSftpAllowed}")"

  userCommand="$(getUserCommand "${username}")"
  userCommand="${userCommand} --shell ${shell}"
  userCommand="${userCommand} ${username}"
  eval "${userCommand}"

  processUserSshConfiguration "${username}" "${userHomeDir}"

  setUserMd5CheckSum "${username}" "${userHomeDir}"
}

#
# Argument parsing
#
while [ $# -gt 0 ]; do
  case "${1}" in
  --data-dir=?*)
    dataDir="${1#*=}"
    ;;
  --output=?*)
    output="${1#*=}"
    ;;
  *)
    echo
    printf "Error: Invalid argument \"%s\"" "${1}"
    echo
    echo
    exit 1
    ;;
  esac
  shift
done

date="$(date +"%Y-%m-%d_%H-%M-%S")"
dataDir="$(realpath "${dataDir}")"

#
# Script start
#
echo "" >> "${output}"
echo "Starting script \"$(realpath "${0}")\" @ ${date}" >> "${output}"
echo "" >> "${output}"

#
# Pre flight check(s)
#
errors=""
if [ "root" != "$(whoami)" ]; then
  errors="${errors}\n- This script must be run as \"root\" user"
fi

if [ ! -d "${dataDir}" ]; then
  errors="${errors}\n- Invalid \"--data-dir\" supplied, directory \"${dataDir}\" does not exist"
fi

which jq >/dev/null
if [ "0" != "${?}" ]; then
  errors="${errors}\n- \"jq\" os package not installed try running \"apt-get install jq\" or your distribution equivalent"
fi

if [ -n "${errors}" ]; then
  echo "Error(s) occurred: ${errors}" >> "${output}"
  echo "" >> "${output}"
  exit 2
fi

#
# Main
#
userFiles="$(getUserFiles "${dataDir}")"
for userFile in ${userFiles}; do
  processUserFile "${dataDir}" "${userFile}"
done

date="$(date +"%Y-%m-%d_%H-%M-%S")"
dataDir="$(realpath "${dataDir}")"

echo "" >> "${output}"
echo "Script \"$(realpath "${0}")\" ended @ ${date}" >> "${output}"
echo "" >> "${output}"
