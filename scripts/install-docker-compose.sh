#!/bin/sh

SUDO=""
if [ "root" != "$(whoami)" ]; then
  SUDO="sudo"
fi

${SUDO} curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
${SUDO} chmod +x /usr/local/bin/docker-compose
${SUDO} ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
${SUDO} curl \
    -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose \
    -o /etc/bash_completion.d/docker-compose