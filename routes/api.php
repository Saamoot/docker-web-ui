<?php

use App\DUI\Json\JsonResponse;
use App\Http\Controllers\Api\V1\ContainersController;
use App\Http\Controllers\Api\V1\ImagesController;
use App\Http\Controllers\Api\V1\NetworksController;
use App\Http\Controllers\Api\V1\VolumesController;
use Illuminate\Support\Facades\Route;

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::any('/', function () {
    return [
        'title'    => 'dui api',
        'versions' => [
            'v1',
        ],
    ];
});

Route::prefix('v1')->name('v1.')->group(function () {
    Route::any('/', function () {
        return [
            'title'     => 'dui api v1',
            'endpoints' => [
                'containers',
                'images',
                'networks',
                'volumes',
            ],
        ];
    });

    create_api_route('images', ImagesController::class)
        ->except('store');
    Route::post('/images/{image?}', [ImagesController::class, 'pull'])
        ->name('images.store');
    Route::post('/images/pull/{image?}', [ImagesController::class, 'pull'])
        ->name('images.pull');

    create_api_route('containers', ContainersController::class)
        ->except(['store', 'update']);
    Route::post('/containers/run/{image?}/{container?}', [ContainersController::class, 'run'])
        ->name('containers.run');
    Route::post('/containers/create/{image?}/{container?}', [ContainersController::class, 'store'])
        ->name('containers.create');
    Route::post('/containers/{image?}/{container?}', [ContainersController::class, 'store'])
        ->name('containers.create');
    Route::post('/containers/{image?}/{container?}', [ContainersController::class, 'store'])
        ->name('containers.store');
    Route::match(['patch', 'put'],'/containers/stop/{container?}', [ContainersController::class, 'stop'])
        ->name('containers.stop');
    Route::match(['patch', 'put'],'/containers/start/{container?}', [ContainersController::class, 'start'])
        ->name('containers.start');
    Route::match(['patch', 'put'],'/containers/restart/{container?}', [ContainersController::class, 'restart'])
        ->name('containers.restart');

    create_api_route('networks', NetworksController::class);
    Route::match(['patch', 'put', 'post'], '/networks/connect/{network?}/{container?}', [NetworksController::class, 'connect'])
        ->name('networks.connect');
    Route::match(['patch', 'put', 'delete'], '/networks/disconnect/{network?}/{container?}', [NetworksController::class, 'disconnect'])
        ->name('networks.disconnect');

    Route::post('/networks/create/{network?}', [NetworksController::class, 'create'])
        ->name('networks.create');

    create_api_route('volumes', VolumesController::class);
    Route::post('/volumes/create/{volume?}', [VolumesController::class, 'create'])
        ->name('volumes.create');
    Route::get('/volumes/files/{volume?}', [VolumesController::class, 'files'])
        ->name('volumes.files');
});

Route::fallback(function (){
    return JsonResponse::getInstance()
        ->setMessage(__('Requested endpoint \':endpoint\' not found', ['endpoint' => url()->current()]))
        ->setResponseCode(404)
        ->setData([
            'method' => request()->method(),
            'protocol' => request()->getScheme(),
            'host' => request()->getHost(),
            'url' => request()->getRequestUri(),
            'query' => request()->getQueryString(),
        ])
        ->toJson();

})->name('not-found');