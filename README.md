# DUI

## todo
1. ~~use json classes for response in image controller~~
2. ~~check if store/pull, delete/remove image controller enpoints returns correct json response~~
3. ~~add controller implementations for: networks, containers, volumes~~
4. fix container user does not have permissions to use docker after opening(exec) shell without executing `newgrp docker`
5. ~~fix/resolve issue when commands fails with handle-able error e.g.~~
```
command: docker network connect postman-network dui-web
error: Error response from daemon: endpoint with name dui-web already exists in network postman-network
```
6. ~~add test for~~:
   - ~~endpoint aliases e.g. `api.v1.image.store`/`api.v1.image.pull`~~
7. ~~fix url parameter names to match in: api route, controller method argument  name, test request data~~
8. test usage of routes with aliased names (create=>store,remove=>destroy)
9. test usage of routes with explicit name instead of http methods, these should have same functionality:
   - POST[data:{}]: `api/v1/networks/store/{networkName}`
   - POST[data:{name: {networkName}] `api/v1/networks/store`
10. write test for classes in `/app/DUI/DockerCommand`
   - ~~fix problem with options if present - pass same option multiple times (port, environment variable) => option name
     cannot be used as key~~ - fixed, tested
11. ~~remove redundant api controller methods (create, remove) - use standard methods~~
    - ~~keep endpoint names !~~

- ???
- profit
