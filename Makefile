instance: images
	export USER_ID="$(shell id -u)"; \
	export USER_NAME="$(shell id -u -n)"; \
	export GROUP_ID="$(shell id -g)"; \
	export GROUP_NAME="$(shell id -g -n)"; \
	export DOCKER_GROUP_ID="$(shell grep "docker" /etc/group | grep -oE ":[0-9]+:" | grep -oE '[0-9]+' --color=never)"; \
	export DOCKER_GROUP_NAME="docker"; \
	docker-compose up -d
	make changeHostFilesystemPermissions

changeHostFilesystemPermissions:
	sudo chown -R $(shell id -un):www-data .
	sudo chmod -R g+w ./storage
	sudo chmod -R +s ./storage

images: buildWebImage

removeInstance:
	docker-compose down --time 0 -v --remove-orphans

attachToContainerAsRoot:
	docker-compose exec -u 0:0 web bash
	make changeHostFilesystemPermissions

attachToContainerAsUser:
	docker-compose exec -u $(shell id -un):www-data web bash
	make changeHostFilesystemPermissions

composerCommand:
	docker run -it --rm -u $(shell id -u):www-data -v $(shell pwd):/app -w /app composer "$${DC_COMMAND-bash}"
	make changeHostFilesystemPermissions

composerDumpAutoload:
	DC_COMMAND="dump-autoload" make composerCommand

composerInstall:
	DC_COMMAND="install" make composerCommand

ideHelpers:
	DC_COMMAND="ide-helper:models -RW" make artisanCommand
	DC_COMMAND="ide-helper:meta" make artisanCommand
	DC_COMMAND="ide-helper:generate" make artisanCommand
	DC_COMMAND="ide-helper:eloquent" make artisanCommand
	make changeHostFilesystemPermissions

execSchedule:
	DC_COMMAND="schedule:run" make artisanCommand

artisanCommand:
	docker-compose exec -u "$${DC_USER:-$(shell id -u):www-data}" web bash -c "php artisan $${DC_COMMAND:-help}"

migrateFresh:
	docker-compose exec -u $(shell id -u):www-data web touch database/database.sqlite
	make changeHostFilesystemPermissions
	DC_COMMAND="migrate:fresh --seed" make artisanCommand

listRoutes:
	DC_COMMAND="route:list $${arguments:-}" make artisanCommand


phpUnitTests:
	@if [ -n "$$filter" ]; then \
  		docker-compose exec -u "$${DC_USER:-$(shell id -u):www-data}" web bash -c "./vendor/bin/phpunit --filter $$filter $$arguments"; \
	else \
	  	docker-compose exec -u "$${DC_USER:-$(shell id -u):www-data}" web bash -c "./vendor/bin/phpunit $$arguments"; \
	fi

phpUnitTestsWithCoverage:
	arguments="--coverage-text" make phpUnitTests

test: phpUnitTests

testWithCoverage: phpUnitTestsWithCoverage

buildWebImage:
	export USER_ID="$(shell id -u)"; \
	export USER_NAME="$(shell id -u -n)"; \
	export GROUP_ID="$(shell id -g)"; \
	export GROUP_NAME="$(shell id -g -n)"; \
	export DOCKER_GROUP_ID="$(shell grep "docker" /etc/group | grep -oE ":[0-9]+:" | grep -oE '[0-9]+' --color=never)"; \
	export DOCKER_GROUP_NAME="docker"; \
	docker-compose build web